tell application "System Events"
	
	set previousSong to ""
	
	repeat
		if exists process "iTunes" then
			tell application "iTunes"
				if player state is playing then
					set currentSong to name of current track
					set currentArtist to artist of current track
					if currentSong is not previousSong then
						set previousSong to currentSong
						
						-- Execute your script, command, or application here                        
						do shell script "/Users/beto/wbanner.sh " & quoted form of currentSong & space & quoted form of currentArtist
						
					end if
					
				end if
			end tell
			
			-- Wait some seconds
			delay 10
		else
			exit repeat
		end if
	end repeat
	
end tell

