# Wifi banner

Wifi banner + AppleScript display song and artist

![banner](https://gitlab.com/betojsp/wifibanner/raw/master/wbanner.gif)


## Built With

* [Etechpath](http://www.etechpath.com/how-to-control-max7219-led-matrix-with-esp8266-wifi-module/) - How to Control MAX7219 LED Matrix with ESP8266 WiFi Module
* [AppleScript](https://apple.stackexchange.com/questions/215801/execute-arbitrary-script-when-itunes-plays-new-song) - Execute arbitrary script when iTunes plays new song?

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
